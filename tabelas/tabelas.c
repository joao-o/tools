#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define EXT ".txtbl"
#define BSZ 150

typedef struct list list;

typedef union car {
   double dval;
   void *pval;
} car;

/* estrutura de suporte para listas 
 * tipos:
 * 0 - double
 * 1 - ponteiro genérico (geralmente para outras listas)
 * 2 - ponteiro para char ( parecido com uma string mas não)
 */

struct list {
   car data;
   char type; 
   list *next;
};

/*funções das listas de doubles*/
void l_push(list ** lst, car data, char type)
{
   while (*lst != NULL) {
      lst = &(*lst)->next;
   }
   *lst = malloc(sizeof(list));
   (*lst)->data = data;
   (*lst)->next = NULL;
   (*lst)->type = type;
}

void l_rm(list * lst)
{
   if (lst != NULL) {
      if (lst->next != NULL)
	 l_rm(lst->next);
      switch(lst->type){
         case 1:
	    l_rm(lst->data.pval);
            break;
         case 2:
            free(lst->data.pval);
            break;
      }
      free(lst);
   }
}

int l_sz(list * lst)
{
   int i;
   for (i = 0; lst != NULL; i++)
      lst = lst->next;
   return i;
}


/* pode ser utilizada em todos os tipos de listas para as quais
 * existe um tipo definido
 * note se que definir um tipo passa também 
 * por escever uma rotina de exibição no codigo seguinte;
 * principalmente para testes*/
void l_print(list * lst)
{
   puts("----");
   while (lst != NULL) {
      switch (lst->type) {
      case 0:
	 printf("%f\n", lst->data.dval);
	 break;
      case 1:
	 l_print(lst->data.pval);
	 break;
      case 2:
	 puts(lst->data.pval);
	 break;
      }
      if(lst->next != NULL) puts("===");
      lst = lst->next;
   }
   puts("----");
}

void l_split(list *lst,list **llist, int parts)
{
   int i, j, s = l_sz(lst) / parts + 1;

   llist[0]=lst;

   for (j = 1; j < parts; j++) {
       lst = llist[j - 1];
       for (i = 1; lst->next != NULL && i < s; i++) {
	 lst = lst->next;
       }
       llist[j]=lst->next;
       lst->next=NULL;
   }
}

void l_rec(list *lst, int parts)
{   
    int i;
    if (parts < 2)
        return;
    list *llist[parts],*llbak[parts];
    l_split(lst,llist,parts);
    memcpy(llbak,llist,sizeof(list*)*parts);

    list **subl = &llist[0];
    while (lst != NULL) {
       subl =(list**)&(lst->data.pval);
       i=1;
       do {
          while (*subl != NULL) {
             subl = &((*subl)->next);
          }
          *subl=llist[i]->data.pval;
          llist[i]->type=0;
          llist[i]=llist[i]->next;
          i++;
       } while(llist[i]!=NULL && i < parts);
       lst=lst->next;
    }
    for (i=1;i<parts;i++)
        l_rm(llbak[i]);
}

/*função de substituição de extensão de um ficheiro*/
char *extsub(char *s, char *ext)
{
   char *r;
   int i;

   for (i = 0; s[i] != 0 && s[i] != '.'; i++);

   r = malloc(i + 1 + strlen(ext));

   strcpy(r, s);
   strcpy(r + i, ext);

   return r;
}

/* diz o numero de linhas num ficheiro*/
int getlns(FILE * fp)
{
   char c;
   int i = 0;
   while ((c = fgetc(fp)) != EOF)
      if (c == '\n')
	 i++;
   return i + 1;
}

/*processa cada linha numa lista de doubles*/
int parseln(FILE * fp, list ** lst)
{
   car tmp;
   char buf[BSZ], *endptr, *ptr;

   endptr = buf;
   if (fgets(buf, BSZ, fp) == NULL)
      return 1;
   do {
      ptr = endptr;
      tmp.dval = strtod(ptr, &endptr);
      if (endptr != ptr)
	 l_push(lst, tmp, 0);
   }
   while (ptr != endptr);
   return 0;
}

/*proocessa um ficheiro numa lista de listas de doubles*/
void parsefile(FILE * fp, list ** lst)
{
   list *tlst = NULL;
   car lpt;
   while (!parseln(fp, &tlst)) {
      lpt.pval = tlst;
      l_push(lst, lpt, 1);
      tlst = NULL;
   }
}

void printfile(FILE * fp, list * dat, list * fmt)
{
    list *subl, *fmti;
    char tmp[BSZ];
    while( dat != NULL ) {
       subl=(list*)dat->data.pval;
       fmti=fmt;
       while(subl != NULL){
          fprintf(fp,(char*)fmti->data.pval,subl->data.dval);
          fmti=fmti->next;
          subl=subl->next;
       }
       if (fmti!=NULL)  
          fprintf(fp,(char*)fmti->data.pval,0.);
       
       fputc('\n',fp);
       dat=dat->next;
    }
}

void parsefmt(FILE *fp, list **lst)
{
   char buf[BSZ];
   int i, j, k;
   car tmp;

   fgets(buf, BSZ, fp);
   for (i = 0; buf[i] != '\n'; i++);
   buf[i] = 0;
   i = 0;

   while (buf[i] != '%' && buf[i] != 0){
      i++;
   } 

   j = 0; 
   do {
      i++;
      if (buf[i] != '%' && buf[i] != 0) continue;
      tmp.pval = malloc(sizeof(char) * (i - j) + 1);
      memcpy(tmp.pval,buf+j, i - j);
      ((char*)(tmp.pval))[i-j] = 0;
      l_push(lst, tmp, 2);
      j = i;
   } while (buf[i] != 0);
}

void help() {
  printf("programa de tratamento de dados tabulares\n"
         "funcionamento:\n"
         "1:  $ tbl <nº de colunas a obter>\n"
         "2:  $ tbl <nº de colunas a obter> <ficheiro a ler>\n"
         "3:  $ tbl <nº de colunas a obter> <ficheiro a ler> <ficheiro a escrever>\n"
         "o programa pode por dados do tipo:\n"
         "x1 y1 z1\n"
         "x1 y2 z2\n"
         "...     \n"
         "xn yn zn\n\n"
         "no tipo:\n"
         "x1 y1 z1 x(n/col) y(n/col) z(n/col)\n"
         "....\n\n"
         "onde n é o número de colunas a obter\n"
         "o formato de escrita dos dados é lido sa primeira linha do ficheiro a\n"
         "tratar. É uma string do estilo printf que pode ter texto entre cada número.\n"
         "Deve ser exercido cuidado com o uso de %% nesta string\n"
         "isto é se tentares escrever um \"%%%%\" o prograva provavelmente dará resultados\n"
         "errados ou crashará\n");

}

int main(int argc, char **argv)
{
   FILE *fpr, *fpw;
   char *n;
   list *lst, *fmtl;
   lst = fmtl = NULL;
   int split;

   switch (argc) {
   case 2:
	 fpr = stdin;
	 fpw = stdout;
	 break;
   case 3:
	 fpr = fopen(argv[2], "r");
	 fpw = fopen (n=extsub(argv[2], EXT),"w");
	 free(n);
	 break;
   case 4:
	 fpr = fopen(argv[2], "r");
	 fpw = fopen(argv[3], "w");
         break;
   default:
	 help();
	 exit(-1);
   }
   split=atoi(argv[1]);

   if (fpr == NULL) {
      puts("?");
      exit(-1);
   }

   parsefmt(fpr, &fmtl);
   parsefile(fpr, &lst);
   l_rec(lst,split);

   printfile(fpw,lst,fmtl);
   l_rm(lst);
   l_rm(fmtl);
   return 0;
}
