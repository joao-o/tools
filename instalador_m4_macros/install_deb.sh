#!/bin/sh

wget https://ece.uwaterloo.ca/~aplevich/Circuit_macros/Circuit_macros.tar.gz
tar -xzf Circuit_macros.tar.gz
cd Circuit_macros8.0
mkdir -p /usr/share/circuit_macros
cp *.m4 /usr/share/circuit_macros/
mkdir -p /usr/share/texmf/tex/latex/circuit_macros
cp boxdims.sty /usr/share/texmf/tex/latex/circuit_macros
texhash
cd ..
rm -r Circuit_macros8.0 Circuit_macros.tar.gz
